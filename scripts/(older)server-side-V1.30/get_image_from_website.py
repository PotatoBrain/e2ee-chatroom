import requests
import base64
import sys


def prepare_html(image_dir=False, link=False, img_tag=''):
    try:
        if image_dir != '0':
            image = open(image_dir, 'rb').read()
        elif link != '0':
            img_tag = '<a href="{0}">'.format(link)
            image = requests.get(link).content

        data_uri = base64.b64encode(image).decode('utf-8')
        img_tag += '<img src="data:image/png;base64,{0}" width="400" height="200">'.format(data_uri)
        print(img_tag)
    except requests.exceptions.MissingSchema:
        print('False')

prepare_html(sys.argv[1], sys.argv[2])
